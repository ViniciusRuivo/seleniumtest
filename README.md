# README #

### Qual o objetivo desse projeto? ###

Projeto para teste prática da Accenture

### Como executar ###

* É possível escolher em qual navegador deseja executar, alterando o parêmetro para Chrome, Firefox ou Edge no arquivo "src/test/resources/features/Tricentis.feature"
* O projeto foi desenvolvido em Maven, então é possível de importar tanto no Eclipse quanto no IntelliJ
* Para iniciar a automação, basta executar a classe "src/test/java/runner/Runner.java"