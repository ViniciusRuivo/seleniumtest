package hooks;


import io.cucumber.java.After;
import io.cucumber.java.Before;

import static webDriverConfig.DriverWeb.quitWebDriver;

public class Hook {

    @Before
    public void init() {

    }

    @After
    public void end() {

        quitWebDriver();
    }
}