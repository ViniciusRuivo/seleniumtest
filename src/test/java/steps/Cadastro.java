package steps;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import pageObject.CadastroPO;

public class Cadastro {

    CadastroPO cadastroPO;

    @Dado("que acesse o site {string} no navegador {string}")
    public void queAcesseOSite(String url, String navegador) {

        cadastroPO = new CadastroPO(navegador);
        cadastroPO.abrirNavegadorNaPagina(url);
    }

    @E("preencha o formulario na aba {string}")
    public void preenchaOFormularioNaAba(String aba) {

        cadastroPO.preencherFormmulario(aba);
    }

    @Quando("eu clicar no botao Send")
    public void euClicarNoBotaoSend() {

        cadastroPO.clicarBotaoSend();
    }

    @Entao("eu valido que exibiu a mensagem {string}")
    public void euValidoQueExibiuAMensagem(String mensagem) {

        cadastroPO.validarMensagem(mensagem);
    }
}