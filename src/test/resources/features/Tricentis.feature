#language: pt

  Funcionalidade: Teste Selenium Accenture

  Cenario: Preencher dados e enviar
    Dado que acesse o site "http://sampleapp.tricentis.com/101/app.php" no navegador "Chrome"
    E preencha o formulario na aba "Enter Vehicle Data"
    E preencha o formulario na aba "Enter Insurant Data"
    E preencha o formulario na aba "Enter Product Data"
    E preencha o formulario na aba "Select Price Option"
    E preencha o formulario na aba "Send Quote"
    Quando eu clicar no botao Send
    Entao eu valido que exibiu a mensagem "Sending e-mail success!"