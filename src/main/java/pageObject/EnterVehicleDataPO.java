package pageObject;

import org.openqa.selenium.By;
import utils.DataYaml;
import webDriverConfig.DriverWeb;

import java.util.Map;

import static utils.Acoes.clickOnElement;
import static utils.Acoes.preencherCampo;

public class EnterVehicleDataPO {

    public EnterVehicleDataPO(DriverWeb web){

        this.web = web;
    }

    DriverWeb web;

    private By campoMake = By.id("make");
    private By campoModel = By.id("model");
    private By campoEnginePerformance = By.id("engineperformance");
    private By campoDateOfManufacture = By.id("dateofmanufacture");
    private By campoNumberSeats = By.id("numberofseats");
    private By campoCylinderCapacity = By.id("cylindercapacity");
    private By rightHandDriveYES = By.xpath("//input[@id='righthanddriveyes']//..//span[@class='ideal-radio']");
    private By campoNumberSeatsMotocycle = By.id("numberofseatsmotorcycle");
    private By campoFuelType = By.id("fuel");
    private By campoPayload = By.id("payload");
    private By campoTotalWeight = By.id("totalweight");
    private By campoListPrice = By.id("listprice");
    private By campoLicensePlateNumber = By.id("licenseplatenumber");
    private By campoAnnualMileage = By.id("annualmileage");
    private By btnNext = By.id("nextenterinsurantdata");


    public void setVehicleData(){

        Map<String,String> enterVehicle = DataYaml.getMapYamlValues("EnterVehicle", "Enter_Vehicle");

        preencherCampo(web.getDriver(), campoMake, enterVehicle.get("make"));
        preencherCampo(web.getDriver(), campoModel, enterVehicle.get("model"));
        preencherCampo(web.getDriver(), campoCylinderCapacity, enterVehicle.get("cylinder_capacity"));
        preencherCampo(web.getDriver(), campoEnginePerformance, enterVehicle.get("engine_performance"));
        preencherCampo(web.getDriver(), campoDateOfManufacture, enterVehicle.get("date_manufacture"));
        preencherCampo(web.getDriver(), campoNumberSeats, enterVehicle.get("number_seats"));
        clickOnElement(web.getDriver(), rightHandDriveYES);
        preencherCampo(web.getDriver(), campoNumberSeatsMotocycle, enterVehicle.get("number_seats_motocycle"));
        preencherCampo(web.getDriver(), campoFuelType, enterVehicle.get("fuel_type"));
        preencherCampo(web.getDriver(), campoPayload, enterVehicle.get("payload"));
        preencherCampo(web.getDriver(), campoTotalWeight, enterVehicle.get("total_weight"));
        preencherCampo(web.getDriver(), campoListPrice, enterVehicle.get("list_price"));
        preencherCampo(web.getDriver(), campoLicensePlateNumber, enterVehicle.get("license_plate_number"));
        preencherCampo(web.getDriver(), campoAnnualMileage, enterVehicle.get("annual_mileage"));
        clickOnElement(web.getDriver(), btnNext);
    }
}
