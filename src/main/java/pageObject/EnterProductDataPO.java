package pageObject;

import org.openqa.selenium.By;
import utils.DataYaml;
import webDriverConfig.DriverWeb;

import java.util.Map;

import static utils.Acoes.clickOnElement;
import static utils.Acoes.preencherCampo;

public class EnterProductDataPO {

    public EnterProductDataPO(DriverWeb web){

        this.web = web;
    }

    DriverWeb web;

    private By campoStartDate = By.id("startdate");
    private By campoInsuranceSum = By.id("insurancesum");
    private By campoMeritRating = By.id("meritrating");
    private By campoDamageInsurance = By.id("damageinsurance");
    private By opcaoEuroProtection = By.xpath("//input[@id='EuroProtection']//..//span[@class='ideal-check']");
    private By campoCourtesyCar = By.id("courtesycar");
    private By btnNext = By.id("nextselectpriceoption");

    public void setProductData(){

        Map<String,String> product = DataYaml.getMapYamlValues("Product", "Product");

        preencherCampo(web.getDriver(), campoStartDate, product.get("start_date"));
        preencherCampo(web.getDriver(), campoInsuranceSum, product.get("insurance_sum"));
        preencherCampo(web.getDriver(), campoMeritRating, product.get("merit_rating"));
        preencherCampo(web.getDriver(), campoDamageInsurance, product.get("damage_insurance"));
        clickOnElement(web.getDriver(), opcaoEuroProtection);
        preencherCampo(web.getDriver(), campoCourtesyCar, product.get("courtesy_car"));
        clickOnElement(web.getDriver(), btnNext);
    }
}
