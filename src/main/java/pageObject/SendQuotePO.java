package pageObject;

import org.junit.Assert;
import org.openqa.selenium.By;
import utils.DataYaml;
import webDriverConfig.DriverWeb;

import java.util.Map;

import static utils.Acoes.*;
import static utils.Verifications.waitElementToBeVisible;

public class SendQuotePO {

    public SendQuotePO(DriverWeb web) {

        this.web = web;
    }

    DriverWeb web;

    private By campoEmail = By.id("email");
    private By campoPhone = By.id("phone");
    private By campoUserName = By.id("username");
    private By campoPassword = By.id("password");
    private By campoConfirmPassword = By.id("confirmpassword");
    private By btnSendEmail = By.id("sendemail");
    private By txtSendingEmailSuccess = By.xpath("//h2");

    public void setSendQuote() {

        Map<String, String> sendQuote = DataYaml.getMapYamlValues("SendQuote", "Send_Quote");

        preencherCampo(web.getDriver(), campoEmail, sendQuote.get("email"));
        preencherCampo(web.getDriver(), campoPhone, sendQuote.get("phone"));
        preencherCampo(web.getDriver(), campoUserName, sendQuote.get("user_name"));
        preencherCampo(web.getDriver(), campoPassword, sendQuote.get("password"));
        preencherCampo(web.getDriver(), campoConfirmPassword, sendQuote.get("password"));
    }

    public void clicarBotaoSend() {

        clickOnElement(web.getDriver(), btnSendEmail);
    }

    public void validarMensagem(String mensagem) {

        waitElementToBeVisible(web.getDriver(), txtSendingEmailSuccess);
        Assert.assertEquals(mensagem, getText(web.getDriver(), txtSendingEmailSuccess));
    }
}
