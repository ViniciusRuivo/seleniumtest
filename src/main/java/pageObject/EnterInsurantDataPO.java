package pageObject;

import org.openqa.selenium.By;
import utils.DataYaml;
import webDriverConfig.DriverWeb;

import java.util.Map;

import static utils.Acoes.clickOnElement;
import static utils.Acoes.preencherCampo;

public class EnterInsurantDataPO {

    public EnterInsurantDataPO(DriverWeb web){

        this.web = web;
    }

    DriverWeb web;

    private By campoFirstName = By.id("firstname");
    private By campoLastName = By.id("lastname");
    private By campoBirthDate = By.id("birthdate");
    private By opcaoMale = By.xpath("//input[@id='gendermale']//..//span[@class='ideal-radio']");
    private By campoCountry = By.id("country");
    private By campoZipCode = By.id("zipcode");
    private By campoOccupation = By.id("occupation");
    private By opcaoBungeeJumping = By.xpath("//input[@id='bungeejumping']//..//span[@class='ideal-check']");
    private By btnNext = By.id("nextenterproductdata");

    public void setInsurantData(){

        Map<String,String> insurant = DataYaml.getMapYamlValues("Insurant", "Insurant");

        preencherCampo(web.getDriver(), campoFirstName, insurant.get("first_name"));
        preencherCampo(web.getDriver(), campoLastName, insurant.get("last_name"));
        preencherCampo(web.getDriver(), campoBirthDate, insurant.get("birth_date"));
        clickOnElement(web.getDriver(), opcaoMale);
        preencherCampo(web.getDriver(), campoCountry, insurant.get("country"));
        preencherCampo(web.getDriver(), campoZipCode, insurant.get("zip_code"));
        preencherCampo(web.getDriver(), campoOccupation, insurant.get("occupation"));
        clickOnElement(web.getDriver(), opcaoBungeeJumping);
        clickOnElement(web.getDriver(), btnNext);
    }
}