package pageObject;

import webDriverConfig.DriverWeb;

import static constantes.Navegador.*;

public class CadastroPO {

    DriverWeb web;

    public CadastroPO(String navegador) {

        if (navegador.equals("Chrome"))
            web = DriverWeb.getDriverWebInstance(CHROME);
        else if (navegador.equals("Firefox"))
            web = DriverWeb.getDriverWebInstance(FIREFOX);
        else if (navegador.equals("Edge"))
            web = DriverWeb.getDriverWebInstance(EDGE);
    }

    SendQuotePO sendQuotePO;

    public void abrirNavegadorNaPagina(String url) {

        web.openBrowserIn(url);
    }

    public void preencherFormmulario(String aba) {

        if (aba.equals("Enter Vehicle Data"))
            new EnterVehicleDataPO(web).setVehicleData();
        else if (aba.equals("Enter Insurant Data"))
            new EnterInsurantDataPO(web).setInsurantData();
        else if (aba.equals("Enter Product Data"))
            new EnterProductDataPO(web).setProductData();
        else if (aba.equals("Select Price Option"))
            new SelectPriceOptionPO(web).setPriceOption();
        else if (aba.equals("Send Quote")) {

            sendQuotePO = new SendQuotePO(web);
            sendQuotePO.setSendQuote();
        }
    }

    public void clicarBotaoSend() {

        sendQuotePO.clicarBotaoSend();
    }

    public void validarMensagem(String mensagem) {

        sendQuotePO.validarMensagem(mensagem);
    }
}
