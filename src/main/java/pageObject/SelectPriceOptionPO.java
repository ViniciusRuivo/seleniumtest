package pageObject;

import org.openqa.selenium.By;
import webDriverConfig.DriverWeb;

import static utils.Acoes.clickOnElement;

public class SelectPriceOptionPO {

    public SelectPriceOptionPO(DriverWeb web) {

        this.web = web;
    }

    DriverWeb web;

    private By opcaoUltimate = By.xpath("//input[@id='selectultimate']//..//span[@class='ideal-radio']");
    private By btnNext = By.id("nextsendquote");

    public void setPriceOption(){

        clickOnElement(web.getDriver(), opcaoUltimate);
        clickOnElement(web.getDriver(), btnNext);
    }
}
