package utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

import static utils.Verifications.waitElementToBeClickable;
import static utils.Verifications.waitElementToBeVisible;

public class Acoes {

    public static void clickOnElement(WebDriver driver, By by) {

        waitElementToBeClickable(driver, by);
        moverAteElemento(driver, by);
        driver.findElement(by).click();
    }

    public static void preencherCampo(WebDriver driver, By by, String txt) {

        waitElementToBeClickable(driver, by);
        moverAteElemento(driver, by);
        driver.findElement(by).sendKeys(txt);
    }

    public static String getText(WebDriver driver, By by) {

        waitElementToBeVisible(driver, by);
        moverAteElemento(driver, by);
        return driver.findElement(by).getText();
    }

    public static void moverAteElemento(WebDriver driver, By by) {

        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(by));
    }
}
