package utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class Verifications {

    private static int TIME_OUT = 30;
    private static WebDriverWait webDriverWait;

    public static void waitElementToBeClickable(WebDriver driver, By by){

        webDriverWait = new WebDriverWait(driver, Duration.ofSeconds(TIME_OUT));
        webDriverWait.until(ExpectedConditions.elementToBeClickable(by));
    }

    public static void waitElementToBeVisible(WebDriver driver, By by){

        webDriverWait = new WebDriverWait(driver, Duration.ofSeconds(TIME_OUT));
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }
}
