package utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

public class DataYaml {

    private static File getYamlDataFile(String fileName){

        return new File("./src/test/resources/datas/"+fileName+".yml");
    }

    public static LinkedHashMap<String,String> getMapYamlValues(String fileName, String titulo){

        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        mapper.findAndRegisterModules();
        Map<String, Object> maps = null;
        try {
            maps = (LinkedHashMap<String,Object>) mapper.readValue(getYamlDataFile(fileName), Map.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return (LinkedHashMap<String, String>) maps.get(titulo);
    }
}
