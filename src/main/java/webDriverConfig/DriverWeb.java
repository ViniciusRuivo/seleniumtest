package webDriverConfig;

import constantes.Navegador;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import java.time.Duration;

import static constantes.Navegador.*;

public class DriverWeb {

    private static WebDriver driver = null;
    private static DriverWeb driverWeb;
    private static int TIME_OUT = 30;

    public DriverWeb(Navegador navegador) {

        String PATH = ".src/test/resources/drivers";

        if (navegador.equals(CHROME)) {
            ChromeOptions options = new ChromeOptions();
            WebDriverManager.chromedriver().config().setCachePath(PATH);
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver(options);
        } else if (navegador.equals(FIREFOX)) {
            FirefoxOptions options = new FirefoxOptions();
            WebDriverManager.firefoxdriver().config().setCachePath(PATH);
            WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver(options);
        } else if (navegador.equals(EDGE)) {
            EdgeOptions options = new EdgeOptions();
            WebDriverManager.edgedriver().config().setCachePath(PATH);
            WebDriverManager.edgedriver().setup();
            driver = new EdgeDriver(options);
        }

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(TIME_OUT));
    }

    public static DriverWeb getDriverWebInstance(Navegador navegador) {

        if (driver == null)
            driverWeb = new DriverWeb(navegador);
        return driverWeb;
    }

    public void openBrowserIn(String url) {

        driver.get(url);
    }

    public static void quitWebDriver() {

        if (driver != null) {
            driver.close();
            driver.quit();
            driver = null;
        }
    }

    public WebDriver getDriver(){

        return driver;
    }
}